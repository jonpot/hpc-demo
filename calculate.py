import sys

def fibonacci(n):
    a = 0
    b = 1
    if n < 0:
        print("Incorrect input")
    elif n == 0:
        return a
    elif n == 1:
        return b
    else:
        for i in range(2,n):
            c = a + b
            a = b
            b = c
        return b

if len(sys.argv) == 2:
    print(fibonacci(int(sys.argv[1])))
else:
    print("Program requires an argument.")
    print("Usage: python3 calculate.py <terms>")
