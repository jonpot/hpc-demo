#!/bin/bash
# The interpreter used to execute the script

#“#SBATCH” directives that convey submission options:

#SBATCH --job-name=demo
#SBATCH --mail-type=BEGIN,END
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=1000m
#SBATCH --time=00:30
#SBATCH --account=support
#SBATCH --partition=standard

# The application(s) to execute along with its input arguments and options:

python3 $HOME/hpc-demo/calculate.py $terms
