# HPC Demo

[Get a user login and account](https://arc-ts.umich.edu/login-request/) on one of the HPC clusters, like Great Lakes.

```bash
# Once you have a user login, ssh into Great Lakes with your user login.
$ ssh jonpot@greatlakes.arc-ts.umich.edu

# Clone this repo.
$ git clone git@bitbucket.org:jonpot/hpc-demo.git

# Run `my_accounts` to determine your associated accounts. Select the account
# with which you plan to run this example.
$ my_accounts

# Then modify the Slurm batch script `~/hpc-demo/myJob.sh` to use your account.

# Run the job.
$ JOBID=$(sbatch --parsable --export=terms=7123 ~/hpc-demo/myJob.sh)

# Watch the job.
$ squeue -j $JOBID
             JOBID PARTITION     NAME     USER  ACCOUNT ST       TIME  NODES NODELIST(REASON)
           4221229  standard     demo   jonpot  support  R       0:14      1 gl3069

# View job output.
$ cat slurm-$JOBID.out
```

## About the App

A sample app that can be used on HPC.

The app calculates fibonacci sequence and is CPU intensive. It is written Python. The app takes a desired sequence position as input, and then returns the value of the term at the specified sequence position. For example, a request of the 9th term would receive a response of 21.

Example usage:

```
$ python3 calculate.py 9
21
```

## Optional Math Background

A little Fibonacci Sequence background from https://en.wikipedia.org/wiki/Fibonacci_number...

> In mathematics, the Fibonacci numbers are the numbers in the following integer sequence, called the Fibonacci sequence, and characterized by the fact that every number after the first two is the sum of the two preceding ones.

In other words, the sequence can be calculated as *F_{n} = F_{n-1} + F_{n-2} where F_0 = 0 and F_1 = 1*.

For example, the first 9 terms of the sequence are: 0, 1, 1, 2, 3, 5, 8, 13, 21, ...

The table below shows the Fibonacci Numbers and their position in the sequence, e.g., the 4th term, F_3, is 2, the 9th term, F_8, is 21, etc...

|      **Position**      | F_0 | F_1 | F_2 | F_3 | F_4 | F_5 | F_6 | F_7 | F_8 |
|:----------------------:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|  **Fibonacci Number**  |  0  |  1  |  1  |  2  |  3  |  5  |  8  | 13  | 21  |
